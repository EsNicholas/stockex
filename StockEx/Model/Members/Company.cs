﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Members
{
    class Company : Member
    {
        public Stock stock { get; private set; }

        public Company( string name, decimal cash, string symbol, uint sharesCount )
            : base(name, cash)
        {
            if (Market.Stocks.Find(s => s.Symbol == symbol) != null)
                throw new Exception("Stock with this symbol already exists");

            stock = new Stock(
                symbol,
                cash / sharesCount - 0.5m,
                cash / sharesCount + 0.5m,
                sharesCount
                );

            Market.Stocks.Add(stock);
        }

        
    }
}
