﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Members
{
    class Member
    {
        public string Username { get; private set; }
        public decimal Cash { get; private set; }
        public List<Share> ControlledShares { get; private set; }

        public Member(string username, decimal cash)
        {
            Username = username;
            Cash = cash;
            ControlledShares = new List<Share>();
        }

        public Orders.MarketOrder CreateBuyingOrder(string stockSymbol, uint qty)
        {
            Stock stock = Market.FindStock(stockSymbol);

            if (stock == null)
                throw new Exception("Stock symbol does not exist");

            if (stock.MarketPriceBuy * qty > Cash)
                throw new Exception("Not enough cash for this operation");

            Share share = new Share(stock, qty);
            ControlledShares.Add(share);
            return new Orders.MarketOrder(share, Orders.OrderKind.Buy);
        }

        public Orders.MarketOrder CreateSellingOrder(int idx, uint qty)
        {
            if (ControlledShares[idx].Quantity < qty)
                throw new Exception("Member does not possess enough shares");

            Share share = ControlledShares[idx].split(qty);
            return new Orders.MarketOrder(share, Orders.OrderKind.Sell);
        }
    }
}
