﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model
{
    // also needed for expansion of members with MarketMaker class
    static class Market
    {
        public static List<Stock> Stocks;

        static Market()
        {
            Stocks = new List<Stock>();
        }

        public static Stock FindStock(string symbol)
        {
            return Stocks.Find(s => s.Symbol == symbol);
        }
    }
}
