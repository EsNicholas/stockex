﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model
{
    class Stock
    {
        public string Symbol;
        public decimal MarketPriceBuy { get; private set; }
        public decimal MarketPriceSell { get; private set; }
        public uint TotalShares { get; private set; }
        public uint OutstandingShares { get; set; }

        public Stock(string symbol, decimal priceBuy, decimal priceSell, uint shares)
        {
            Symbol = symbol;
            MarketPriceBuy = priceBuy;
            MarketPriceSell = priceSell;
            TotalShares = shares;
            OutstandingShares = 0;
        }
    }
}
