﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    class MarketOrder : Order
    {
        public OrderKind Type { get; private set; }

        public MarketOrder( Share share, OrderKind _type ) : base(share)
        {
            Type = _type;
        }
    }
}
