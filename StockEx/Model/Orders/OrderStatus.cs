﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    enum OrderStatus
    {
        Created,
        Sent,
        Cancelled,
        Finished
    }
}
