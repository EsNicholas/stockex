﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    class OSO_Order : ConditionalOrder
    {
        public OSO_Order(Order co) : base(co.TradingShare)
        {
            m_controllingOrder = co;
            m_controlledOrders = new List<Order>();
        }

        public override bool isConditionMet()
        {
            if(m_controllingOrder.Status == OrderStatus.Finished)
            {
                // todo: add to db
                m_controlledOrders.ForEach(o => o.OnSent());
                return true;
            }
            return false;
        }

        public void AddOrders(params Order[] _orders)
        {
            foreach (Order order in _orders)
            {
                if (order.TradingShare.ControlledStock != TradingShare.ControlledStock)
                    throw new System.Exception("OSO Order: can only add orders on same stock");

                if (order.Status != OrderStatus.Created)
                    throw new System.Exception("OSO Order: can only add unsent orders");

                m_controlledOrders.Add(order);
            }
        }

        private Order m_controllingOrder;
        private List<Order> m_controlledOrders;
    }
}
