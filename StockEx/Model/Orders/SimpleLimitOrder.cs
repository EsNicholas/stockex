﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    class SimpleLimitOrder : LimitOrder
    {
        public SimpleLimitOrder(Share share, OrderKind ok)
            : base(share, ok)
        { }

        public override bool isLimitReached()
        {
            if (Type == OrderKind.Buy)
                return TradingShare.ControlledStock.MarketPriceBuy <= PriceLimit;
            else
                return TradingShare.ControlledStock.MarketPriceSell >= PriceLimit;
        }
    }
}
