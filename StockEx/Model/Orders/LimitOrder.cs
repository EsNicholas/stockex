﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    abstract class LimitOrder : MarketOrder
    {
        public LimitOrder(Share share, OrderKind ok)
            : base(share, ok)
        {
            PriceLimit = 0;
        }

        public override void OnSent()
        {
            if (isLimitReached())
                base.OnSent();
        }

        public decimal PriceLimit { get; set; }

        public abstract bool isLimitReached();

    }
}
