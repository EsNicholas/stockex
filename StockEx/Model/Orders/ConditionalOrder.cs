﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    abstract class ConditionalOrder : Order
    {
        public ConditionalOrder(Share share) : base(share)
        { }

        public abstract bool isConditionMet();
    }
}
