﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    abstract class Order
    {
        public Order(Share share)
        {
            Status = OrderStatus.Created;
            TradingShare = share;
        }

        public void OnCancelled()
        {
            if (Status == OrderStatus.Sent)
                Status = OrderStatus.Cancelled;
            else
                throw new SystemException("Order.Cancel: only sent orders can be cancelled");
        }

        public void OnFinished()
        {
            if (Status == OrderStatus.Sent)
                Status = OrderStatus.Finished;
            else
                throw new SystemException("Order.Finish: only sent orders can be finished");
        }

        public virtual void OnSent()
        {
            if (Status == OrderStatus.Created)
                Status = OrderStatus.Sent;
            else
                throw new SystemException("Order.Send: only created orders can be sent");
        }

        public OrderStatus Status { get; private set; }

        public Share TradingShare { get; private set; }
    }
}
