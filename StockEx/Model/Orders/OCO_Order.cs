﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model.Orders
{
    // OCO - Order Cancels Other
    class OCO_Order : ConditionalOrder
    {
        public OCO_Order(Share share) : base(share)
        {
            m_controlledOrders = new List<Order>();
        }

        public void AddOrdersToControl(params Order[] _orders)
        {
            foreach (Order order in _orders)
            {
                if(order.TradingShare.ControlledStock != TradingShare.ControlledStock)
                    throw new System.Exception("OCO Order: can only add orders on same stock");

                if (order.Status != OrderStatus.Sent)
                    throw new System.Exception("OCO Order: can only add sent orders");

                m_controlledOrders.Add(order);
            }
        }

        public override bool isConditionMet()
        {
            foreach(Order order in m_controlledOrders)
            {
                if (order.Status == OrderStatus.Finished)
                {
                    m_controlledOrders.Remove(order);
                    CancelOtherOrders();
                    return true;
                }
            }
            return false;
        }

        private void CancelOtherOrders()
        {
            //todo: remove from db
            m_controlledOrders.ForEach(o => o.OnCancelled());
        }

        public IEnumerable<Order> GetOrders()
        {
            return m_controlledOrders.AsEnumerable();
        }

        private List<Order> m_controlledOrders;
    }
}
