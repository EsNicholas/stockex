﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx.Model
{
    class Share
    {
        public Stock ControlledStock { get; private set; }
        public uint Quantity { get; private set; }

        public Share(Stock stock, uint quantity)
        {
            if (stock.TotalShares - stock.OutstandingShares < quantity)
            {
                throw new System.Exception(
                    string.Format(
                        "Market does not have enough shares: {0}, {1}",
                        stock.Symbol,
                        quantity
                        )
                    );
            }

            ControlledStock = stock;
            Quantity = quantity;
            stock.OutstandingShares += Quantity;
        }

        public Share split(uint qty)
        {
            if (qty >= Quantity)
                throw new System.Exception("Share.split: new quantity must be less than existing one");

            Quantity -= qty;

            return new Share(ControlledStock, qty);
        }
    }
}
