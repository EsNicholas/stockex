﻿using StockEx.Model.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockEx
{
    class Program
    {
        static void Main(params string[] parameters)
        {
            Model.StockData sd = new Model.StockData();
            TestDataGenerator.fillMembers(sd);
            sd.members.ForEach(Dumper.dump);

            var order = TestDataGenerator.generateTestOrders();
            Dumper.dumpOrder(order[0]);
            order[1].OnFinished();

            OCO_Order oco = order[0] as OCO_Order;
            oco.isConditionMet();
            System.Console.Out.WriteLine("======= AFTER ONE ORDER FINISHED ========");

            Dumper.dumpOrder(order[0]);
        }
    }
}
