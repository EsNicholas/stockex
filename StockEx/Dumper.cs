﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockEx.Model.Orders;
using StockEx.Model.Members;
using StockEx.Model;

namespace StockEx
{
    static class Dumper
    {
        // todo: add visitors
        public static void dump(Member m)
        {
            System.Console.Out.WriteLine("========================");
            System.Console.Out.WriteLine("Member:");
            System.Console.Out.WriteLine("Username: " + m.Username);
            System.Console.Out.WriteLine("Cash: " + m.Cash);

            foreach(Share s in m.ControlledShares)
            {
                dumpShare(s);
            }

            if (m is Company)
                dumpCompany(m as Company);

            System.Console.Out.WriteLine("========================");

        }

        private static void dumpShare(Share s)
        {
            System.Console.Out.WriteLine("Stock: " + s.ControlledStock.Symbol);
            System.Console.Out.WriteLine("QTY: " + s.Quantity);
        }

        private static void dumpCompany(Company c)
        {
            System.Console.Out.WriteLine("Company stock:");
            dumpStock(c.stock);
        }

        private static void dumpStock(Stock stock)
        {
            System.Console.Out.WriteLine( "Symbol: " + stock.Symbol );
            System.Console.Out.WriteLine("Shares: {0}/{1}", stock.OutstandingShares, stock.TotalShares);
            System.Console.Out.WriteLine("Bid/Ask: {0}/{1}", stock.MarketPriceBuy, stock.MarketPriceSell);
        }

        public static void dumpOrder(Order o)
        {
            System.Console.Out.WriteLine("\nOrder:");
            dumpShare(o.TradingShare);
            System.Console.Out.WriteLine("Status:" + o.Status);

            if (o is LimitOrder)
                dumpLimitedOrder(o as LimitOrder);
            else if (o is OCO_Order)
                dumpOCO(o as OCO_Order);
        }

        private static void dumpOCO(OCO_Order oCO_Order)
        {
            System.Console.Out.WriteLine("Order is OCO\n Contains next orders: ");

            foreach (var order in oCO_Order.GetOrders())
            {
                dumpOrder(order);
            }
        }

        private static void dumpLimitedOrder(LimitOrder limitOrder)
        {
            System.Console.Out.WriteLine("Order is limited");
            System.Console.Out.WriteLine("Limit: " + limitOrder.PriceLimit);
        }
    }
}
