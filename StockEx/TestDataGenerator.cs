﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockEx.Model.Members;
using StockEx.Model.Orders;

namespace StockEx
{
    static class TestDataGenerator
    {
        static public void fillMembers(Model.StockData sd)
        {
            Company company1 = new Company("Microsoft Corp.", 2000000.00m, "MSFT", 200000);
            Company company2 = new Company("Apple", 1000000.00m, "APL", 30000);

            Member m1 = new Member("Member", 10000.00m);
            Member m2 = new Member("Steve", 5000.00m);

            m1.CreateBuyingOrder("MSFT", 100);
            m1.CreateBuyingOrder("APL", 50);

            m2.ControlledShares.Add(new Model.Share(Model.Market.FindStock("APL"), 200));
            m2.CreateSellingOrder(0, 100);

            sd.members.Add(m1);
            sd.members.Add(m2);
            sd.members.Add(company1);
            sd.members.Add(company2);
        }

        static public List<Order> generateTestOrders()
        {
            List<Order> orders = new List<Order>();
            Model.Stock stock1 = new Model.Stock("KKK", 50.5m, 51.5m, 1000);
            
            Model.Share share1 = new Model.Share(stock1, 100);
            Model.Share share2 = new Model.Share(stock1, 200);
            Model.Share share3 = new Model.Share(stock1, 300);

            SimpleLimitOrder slo = new SimpleLimitOrder(share1, OrderKind.Sell);
            slo.PriceLimit = 49m;
            
            StopOrder so = new StopOrder(share2, OrderKind.Buy);
            so.PriceLimit = 48m;

            MarketOrder mo = new MarketOrder(share3, OrderKind.Buy);

            slo.OnSent();
            so.OnSent();
            mo.OnSent();

            OCO_Order oco = new OCO_Order(share1);
            oco.AddOrdersToControl(slo, so, mo);

            orders.Add(oco);
            orders.Add(mo);
            return orders;
        }
    }
}
